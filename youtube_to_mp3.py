import ffmpy
import os, shutil

from os import listdir
from os.path import isfile, join
from pytube import YouTube
from time import sleep

video = raw_input("Enter YouTube Video URL:\n")
mypath = 'staging'

def clear_directory():
	folder = mypath
	for the_file in os.listdir(folder):
	    file_path = os.path.join(folder, the_file)
	    try:
	        if os.path.isfile(file_path):
	            os.unlink(file_path)
	        #elif os.path.isdir(file_path): shutil.rmtree(file_path)
	    except Exception as e:
	        print(e)
	sleep(1)

clear_directory()

youtube = YouTube(video)
youtube.streams.first().download(mypath)

filename = [f for f in listdir(mypath) if isfile(join(mypath, f))][0]
filepath = '%s/%s' % (mypath, filename)

title = youtube.title
mp3path = '%s/%s.mp3' % ('music', filename)

ff = ffmpy.FFmpeg(
    inputs={filepath: None},
    outputs={mp3path: None}
)

ff.run()

clear_directory()
raw_input("Done - Press Enter to Exit")